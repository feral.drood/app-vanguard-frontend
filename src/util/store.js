import {createStore} from "redux";
import mainAction from "./reducers";

const store = createStore(mainAction);

export default store;
