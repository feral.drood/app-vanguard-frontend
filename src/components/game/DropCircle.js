import React from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import CardView from "./CardView";
import {getUserHash} from "../../util/auth";

class DropCircle extends React.Component {
    constructor(props) {
        super(props);

        this.handleDropView = this.handleDropView.bind(this);
        this.handleCardMouseOver = this.handleCardMouseOver.bind(this);
    }

    render() {
        let cardParams = {...this.props.card};
        if (!cardParams.image) {
            cardParams.image = '/images/field/drop.png';
        }
        return (
            <div className="card-circle discard">
                <CardView {...cardParams} onMouseOver={this.handleCardMouseOver}/>
                <div className="count">{this.props.count}</div>
                {this.props.count > 0 && <div className="view" onClick={this.handleDropView}><i className="far fa-eye"/></div>}
            </div>
        )
    }

    handleCardMouseOver() {
        if (this.props.card) {
            this.props.dispatch({
                'type': 'card.preview',
                'data': {
                    'image': this.props.card.image,
                    'name': this.props.card.name,
                    'description': this.props.card.description,
                },
            });
        }
    }

    handleDropView() {
        if (Object.keys(this.props.dialog.cards).length === 0) {
            let newActions = [];
            let newTitle = this.props.dialog.title;
            if (Object.keys(this.props.dialog.actions).length > 0) {
                newActions = {close: 'Close drop cards', ...this.props.dialog.actions};
            } else {
                newActions = {close: 'Close drop cards'};
                newTitle = 'Drop cards';
            }

            this.props.dispatch({
                type: 'game.updates',
                data: [
                    {
                        type: 'action.dialog',
                        player: getUserHash(),
                        data: {
                            load_zone: 'drop',
                            load_player_hash: this.props.hash,
                            title: newTitle,
                            actions: newActions,
                        }
                    }
                ]
            });
        }
    }
}

DropCircle.propTypes = {
    hash: PropTypes.string.isRequired,
};

function mapStateToProps(state, ownProps) {
    return {
        count: state['player_' + ownProps.hash + '_drop_count'] ? state['player_' + ownProps.hash + '_drop_count'] : 0,
        card: state['player_' + ownProps.hash + '_drop_card'] ? state['player_' + ownProps.hash + '_drop_card'] : null,
        dialog: state.dialog ? state.dialog : [],
    };
}

export default connect(mapStateToProps)(DropCircle);
