import React from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHeader from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import SelectField from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import {getBuilderCards, saveConstructedDeck} from "../util/api";
import CardPreview from './CardPreview';
import RaisedButton from '@material-ui/core/Button';
import * as routes from "../constants/routes";
import CircularProgress from '@material-ui/core/CircularProgress';

class DeckBuilder extends React.Component {
    constructor(props) {
        super(props);
        let deckHash = null;
        let clanName = 'Royal Paladin';
        if (props.match.params.deck !== undefined) {
            deckHash = props.match.params.deck;
        }
        this.onNumberChange = this.onNumberChange.bind(this);
        this.onRowHover = this.onRowHover.bind(this);
        this.onRowHoverExit = this.onRowHoverExit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onClanChange = this.onClanChange.bind(this);
        this.doDeckSave = this.doDeckSave.bind(this);
        this.state = {
            loading: true,
            deck: deckHash,
            name: '',
            clan: clanName,
            cards: [],
            validName: false,
            sentinelCount: 0,
            triggerCount: 0,
            healTriggerCount: 0,
            cardCount: 0,
        };
    }

    componentDidMount() {
        let component = this;
        let loadId = this.state.clan;
        if (this.state.deck !== null) {
            loadId = this.state.deck;
        }
        getBuilderCards(loadId).then(function (response) {
            let newData = {};
            if (response.data.name) {
                newData.name = response.data.name;
            }
            if (response.data.clan) {
                newData.clan = response.data.clan;
            }
            if (response.data.cards) {
                newData.cards = response.data.cards;
            }
            newData.loading = false;
            component.setState(newData, () => {
                component.validateDeck();
            });
        });
    }

    onClanChange(event, key, newClan) {
        let component = this;
        this.setState(
            {
                'clan': newClan,
                'loading': true
            },
            () => {
                getBuilderCards(newClan).then(function (response) {
                    if (response.data.cards) {
                        component.setState({
                            loading: false,
                            cards: response.data.cards
                        }, () => {
                            component.validateDeck();
                        });
                    }
                })
            }
        );
    }

    onNameChange(event, newName) {
        let component = this;
        this.setState({
            name: newName
        }, () => {
            component.validateDeck();
        });
    }

    validateDeck() {
        let cards = this.state.cards;
        let cardCount = cards.length;
        let total = 0;
        let sentinelCount = 0;
        let triggerCount = 0;
        let healCount = 0;
        let nameRegexp = RegExp('^[a-zA-Z0-9]{3,20}$');

        for (let c=0;c<cardCount;c++) {
            if (cards[c].shield === 0) {
                sentinelCount += cards[c].count;
            }
            if (cards[c].trigger !== null) {
                triggerCount += cards[c].count;
                if (cards[c].trigger === 'heal') {
                    healCount += cards[c].count;
                }
            }
            total += cards[c].count;
        }

        this.setState({
            total: total,
            validName: nameRegexp.test(this.state.name),
            sentinelCount: sentinelCount,
            triggerCount: triggerCount,
            healTriggerCount: healCount,
            cardCount: total,
        });
    }

    onNumberChange(event) {
        let cards = this.state.cards;
        let cardCount = cards.length;

        for (let c=0;c<cardCount;c++) {
            if (cards[c].id === event.target.name) {
                cards[c].count = parseInt(event.target.value, 10) || 0;
                break;
            }
        }

        this.setState({
            cards: cards,
        });
        this.validateDeck();
    }

    onRowHover(rowNumber) {
        if (this.state.loading) {
            return;
        }
        if (this.state.cards[rowNumber].image !== null) {
            this.props.dispatch({
                'type': 'card.preview',
                'data': {
                    'image': this.state.cards[rowNumber].image,
                    'name': this.state.cards[rowNumber].name,
                    'description': this.state.cards[rowNumber].description,
                },
            });
        }
    }

    onRowHoverExit(rowNumber) {
        if (this.state.loading) {
            return;
        }
        if (this.state.cards[rowNumber].image !== null) {
            this.props.dispatch({
                'type': 'card.preview',
                'data': null,
            });
        }
    }

    doDeckSave() {
        let data = {
            name: this.state.name,
            cards: [],
        };

        let cardCount = this.state.cards.length;
        for (let c=0;c<cardCount;c++) {
            if (this.state.cards[c].count > 0) {
                data.cards.push(this.state.cards[c].id + '#' + this.state.cards[c].count);
            }
        }

        saveConstructedDeck(data).then((response) => {
            this.props.history.push(routes.DECK);
        });
    }

    render() {
        let cards = [];
        let validation = [];
        let cardCount = this.state.cards.length;
        if (this.state.loading) {
            cards.push(
                <TableRow key="loading">
                    <TableCell key="loading"><CircularProgress key="progress"/></TableCell>
                </TableRow>
            );
        } else {
            for (let c=0;c<cardCount;c++) {
                let errorText = "";
                if (this.state.cards[c].count > 4) {
                    errorText = 'invalid';
                }
                cards.push(
                    <TableRow key={this.state.cards[c].id}>
                        <TableCell key="name">{this.state.cards[c].name}</TableCell>
                        <TableCell key="grade" style={{width: '20px'}}>{this.state.cards[c].grade}</TableCell>
                        <TableCell key="shield" style={{width: '20px'}}>{this.state.cards[c].shield}</TableCell>
                        <TableCell key="trigger" style={{width: '60px'}}>{this.state.cards[c].trigger}</TableCell>
                        <TableCell key="number" style={{width: '10px'}}>
                            <TextField
                                key="number"
                                type="number"
                                id={this.state.cards[c].id}
                                name={this.state.cards[c].id}
                                defaultValue={this.state.cards[c].count}
                                onChange={this.onNumberChange}
                                errorText={errorText}
                            />
                        </TableCell>
                    </TableRow>
                );
            }
        }

        let invalidDeck = false;
        let nameValidationClass = "validation-ok";
        if (!this.state.validName) {
            nameValidationClass = "validation-error";
            invalidDeck = true;
        }
        validation.push(<li key="name" className={nameValidationClass}>Valid deck name</li>);

        let deckValidationClass = "validation-ok";
        if (this.state.cardCount !== 50) {
            deckValidationClass = "validation-error";
            invalidDeck = true;
        }
        validation.push(<li key="count" className={deckValidationClass}>Deck size {this.state.cardCount}=50</li>);

        let sentinelValidationClass = "validation-ok";
        if (this.state.sentinelCount > 4) {
            sentinelValidationClass = "validation-error";
            invalidDeck = true;
        }
        validation.push(<li key="sentinel" className={sentinelValidationClass}>Sentinel count {this.state.sentinelCount}&lt;=4</li>);

        let triggerValidationClass = "validation-ok";
        if (this.state.triggerCount !== 16) {
            triggerValidationClass = "validation-error";
            invalidDeck = true;
        }
        validation.push(<li key="trigger" className={triggerValidationClass}>Trigger count {this.state.triggerCount}=16</li>);

        let healValidationClass = "validation-ok";
        if (this.state.healTriggerCount > 5) {
            healValidationClass = "validation-error";
            invalidDeck = true;
        }
        validation.push(<li key="heal" className={healValidationClass}>Heal trigger count {this.state.healTriggerCount}&lt;=4</li>);

        return (
            <div className="game-zone">
                <CardPreview/>
                <Paper className="deck-builder">
                <SelectField className="clan-select" value={this.state.clan} onChange={this.onClanChange}>
                    <MenuItem value="Royal Paladin" primaryText="Royal Paladin" />
                    <MenuItem value="Shadow Paladin" primaryText="Shadow Paladin" />
                    <MenuItem value="Gold Paladin" primaryText="Gold Paladin" />
                    <MenuItem value="Oracle Think Tank" primaryText="Oracle Think Tank" />
                    <MenuItem value="Angel Feather" primaryText="Angel Feather" />
                    <MenuItem value="Genesis" primaryText="Genesis" />
                    <MenuItem value="Kagero" primaryText="Kagero" />
                    <MenuItem value="Narukami" primaryText="Narukami" />
                    <MenuItem value="Tachikaze" primaryText="Tachikaze" />
                    <MenuItem value="Nubatama" primaryText="Nubatama" />
                    <MenuItem value="Murakumo" primaryText="Murakumo" />
                    <MenuItem value="Nova Grappler" primaryText="Nova Grappler" />
                    <MenuItem value="Dimension Police" primaryText="Dimension Police" />
                    <MenuItem value="Etranger" primaryText="Etranger" />
                    <MenuItem value="Link Joker" primaryText="Link Joker" />
                    <MenuItem value="Dark Irregulars" primaryText="Dark Irregulars" />
                    <MenuItem value="Spike Brothers" primaryText="Spike Brothers" />
                    <MenuItem value="Pale Moon" primaryText="Pale Moon" />
                    <MenuItem value="Gear Chronicle" primaryText="Gear Chronicle" />
                    <MenuItem value="Granblue" primaryText="Granblue" />
                    <MenuItem value="Bermuda Triangle" primaryText="Bermuda Triangle" />
                    <MenuItem value="Aqua Force" primaryText="Aqua Force" />
                    <MenuItem value="Megacolony" primaryText="Megacolony" />
                    <MenuItem value="Great Nature" primaryText="Great Nature" />
                    <MenuItem value="Neo Nectar" primaryText="Neo Nectar" />
                </SelectField><br/>
                asd<TextField onChange={this.onNameChange} variant="outlined" label="Deck name" value={this.state.name}/><br/>
                <Table onRowHover={this.onRowHover} onRowHoverExit={this.onRowHoverExit}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell style={{width: '20px'}}>Grade</TableCell>
                            <TableCell style={{width: '20px'}}>Shield</TableCell>
                            <TableCell style={{width: '60px'}}>Trigger</TableCell>
                            <TableCell style={{width: '10px'}}>#</TableCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false} showRowHover={true}>
                        {cards}
                    </TableBody>
                    <TableFooter adjustForCheckbox={false}>
                        <TableRow>
                            <TableCell colSpan="4">
                                <ul>
                                    {validation}
                                </ul>
                            </TableCell>
                            <TableCell><RaisedButton label="Save" primary={true} disabled={invalidDeck} onClick={this.doDeckSave}/></TableCell>
                        </TableRow>
                    </TableFooter>
                </Table>
                </Paper>
            </div>
        )
    }
}

export default withRouter(connect(null)(DeckBuilder));
