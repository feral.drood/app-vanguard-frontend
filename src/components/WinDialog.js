import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import FlatButton from '@material-ui/core/Button';
import {connect} from "react-redux";
import {clearGameData, getPlayerHash} from "../util/auth";
import {withRouter} from "react-router-dom";
import * as routes from "../constants/routes";

class WinDialog extends React.Component {
    handleClose = () => {
        this.props.dispatch({
           'type': 'game.clear',
           'player': null
        });
        // setGameId(null);
        // setUserId(null);
        clearGameData();
        this.props.history.push(routes.USER_DECKS);
    };

    render() {
        let open = (this.props.winner !== null && this.props.winner !== undefined && getPlayerHash() !== null);
        let message = (getPlayerHash() === this.props.winner ? 'You have won!' : 'You lost');
        const actions = [
            <FlatButton
                label="Ok"
                primary={true}
                onClick={this.handleClose}
            />
        ];

        return (
            <Dialog
                title={message}
                actions={actions}
                modal={true}
                contentStyle={{width: "300px"}}
                open={open}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        winner: state.win
    };
}

export default withRouter(connect(mapStateToProps)(WinDialog));
