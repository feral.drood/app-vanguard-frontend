import api from '../util/api';

export function getClanList() {
    return api.get('/api/v1/builder/clans');
}

export function getClanCardList(clan) {
    return api.get('/api/v1/builder/clans/' + clan);
}
