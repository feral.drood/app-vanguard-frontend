# Install
cp src/config/index.dist.js src/config/index.js
vim src/config/index.js
npm i

# Build
npm run build

# Dev
cd docker && docker-compose up
docker inspect vanguard-frontend-node | grep IP
write ip to /etc/hosts as vanguard and open http://vanguard:3000

