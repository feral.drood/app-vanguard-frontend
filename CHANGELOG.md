## Unreleased
### Added
- Possibility to switch clans in deck builder

## 1.0.0
### Added
- Initial release
